# Bitbucket Pipelines Pipe: js-install-build-test

Restores packages, runs lint, builds the web application and runs unit tests.

## Variables

| Variable              | Description           |
| --------------------- | ----------------------------------------------------------- |
| WEBAPP_PATH   		| Path to the directory containing the web application |

## YAML Definition (Using repository variables)

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: docker://cepass/js-install-build-test
	variables:
	  WEBAPP_PATH: $WEBAPP_PATH
```

## Support

If you’d like help with this pipe, or you have an issue or feature request, let us know.
The pipe is maintained by pass@certusportautomation.com.

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce
