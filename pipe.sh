#!/usr/bin/env bash

# Define colors used in building logs	 
HIGHLIGHT_YELLOW='\033[0;33m'
HIGHLIGHT_GREEN='\033[0;32m'

# Set high expectations so that Pipelines fails on an error or exit 1
set -e
set -o pipefail

cd $WEBAPP_PATH

printf "${HIGHLIGHT_YELLOW}Setting npm feed access token..."
npm config set "//cepass.pkgs.visualstudio.com/e3fb5f84-dc78-4168-b9ba-fa1074141fcb/_packaging/PASS_NPM/npm/registry/:_password" "$NPM_FEED_ACCESS_TOKEN"
npm config set "//cepass.pkgs.visualstudio.com/e3fb5f84-dc78-4168-b9ba-fa1074141fcb/_packaging/PASS_NPM/npm/:_password" "$NPM_FEED_ACCESS_TOKEN"

printf "${HIGHLIGHT_YELLOW}Installing depencies..."
npm ci

printf "${HIGHLIGHT_YELLOW}Running lint..."
npm run lint

printf "${HIGHLIGHT_YELLOW}Running unit tests..."
npm run test:unit

printf "${HIGHLIGHT_YELLOW}Run a production build..."
npm run build # Run a production build

printf "${HIGHLIGHT_GREEN} Script executed succesfully! \n"
printf $? # exit code